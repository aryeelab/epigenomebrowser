# Based on the shiny-server centos vagrant setup.sh:
# https://github.com/rstudio/shiny-server/tree/master/vagrant/centos6

# This box didn't include root in 'sudoers', so any command with 'sudo' would fail 
# (since it runs as root). Add root to approved sudoers list so it's not a problem.
echo "root            ALL=(ALL)               NOPASSWD: ALL" >> /etc/sudoers

# IPTables is enabled on this box by default. Stop.
sudo /etc/init.d/iptables stop
sudo chkconfig iptables off

# Enable EPEL
rpm -Uvh http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm

# Install R
yum install -y R

wget https://s3.amazonaws.com/rstudio-shiny-server-os-build/centos-6.3/x86_64/VERSION -O "version.txt"
VERSION=`cat version.txt`

# Install the latest SSP build
wget "https://s3.amazonaws.com/rstudio-shiny-server-os-build/centos-6.3/x86_64/shiny-server-$VERSION-x86_64.rpm" -O ss-latest.rpm
yum install --nogpgcheck -y ss-latest.rpm

# Make application timeouts longer
sed -i  's/location \/ {/location \/ {\n    app_init_timeout 300;\n    app_idle_timeout 2592000; # 1 month/' /etc/shiny-server/shiny-server.conf

sudo su - -c "R -e \"install.packages('shiny', repos='http://cran.rstudio.com/')\""
sudo cp -R /usr/lib64/R/library/shiny/examples/* /srv/shiny-server/




## Install epigenome browser
cd /srv/shiny-server

# Install required packages
yum install -y libxml2-devel libcurl-devel git unzip

# Update R packages
R -e "update.packages(repos='http://cran.revolutionanalytics.com', ask=FALSE)"
#R -e "update.packages(ask=FALSE)"
# Set repo to http://cran.revolutionanalytics.com

git clone https://bitbucket.org/aryeelab/epigenomebrowser.git
cd epigenomebrowser
mkdir -p /vagrant/epigenomebrowser_data
ln -s /vagrant/epigenomebrowser_data data
R CMD BATCH setup.R 
