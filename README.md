This is a [Shiny](http://shiny.rstudio.com/) app motivated by the need to view genomic DNA methylation data from many samples (10s to 1000s). The code may also be useful for others looking to display other types of multiple sample genomic data where the measurement is continuous.

Below is a screenshot of the Shiny app running at http://aryee.mgh.harvard.edu/epigenomebrowser with a small subset of TCGA DNA methylation data from the Illumina 450k array:

![epigenome_browser.png](https://bitbucket.org/repo/jGLEkj/images/287712890-epigenome_browser.png)


